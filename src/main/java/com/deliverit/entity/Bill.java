package com.deliverit.entity;

import java.beans.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

import com.deliverit.util.WarningType;

@Entity
@Table(name="bill")
public class Bill implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long 		id;
	private String 		text;
	private BigDecimal 	originalValue;
	private BigDecimal  correctedValue;
	private LocalDate 	dueDate;
	private LocalDate 	payDate;
	private Period 		delayDays;

	public static Bill getInstance(){
		return new Bill();
	}

	public Bill withText(String text){
        this.text = text;
        return this;
    }

	public Bill withOriginalValue(BigDecimal 	originalValue){
        this.originalValue = originalValue;
        return this;
    }

	public Bill withDueDate(LocalDate 	dueDate){
        this.dueDate = dueDate;
        return this;
    }

	public Bill withPayDate(LocalDate 	payDate){
        this.payDate = payDate;
        return this;
    }

	@Id
	@Column(name = "bill_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@SuppressWarnings(WarningType.DEPRECATION)
	@Column(name = "text", nullable=false)
	@NotBlank(message="text é obrigatório!")
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@SuppressWarnings(WarningType.DEPRECATION)
	@Column(name = "original_value", nullable=false)
	@NotBlank(message="original_value é obrigatório!")
	public BigDecimal getOriginalValue() {
		return originalValue;
	}

	public void setOriginalValue(BigDecimal originalValue) {
		this.originalValue = originalValue;
	}

	@SuppressWarnings(WarningType.DEPRECATION)
	@Column(name = "due_date", nullable=false)
	@NotBlank(message="due_date é obrigatório!")
	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

	@SuppressWarnings(WarningType.DEPRECATION)
	@Column(name = "pay_date", nullable=false)
	@NotBlank(message="pay_date é obrigatório!")
	public LocalDate getPayDate() {
		return payDate;
	}

	public void setPayDate(LocalDate payDate) {
		this.payDate = payDate;
	}

	@Column(name = "delay_days", nullable=false)
	public Period getDelayDays() {
		return this.delayDays;
	}

	public void setDelayDays(Period delayDays) {
		this.delayDays = Period.between(this.getDueDate(), this.getPayDate());;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((delayDays == null) ? 0 : delayDays.hashCode());
		result = prime * result + ((dueDate == null) ? 0 : dueDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((originalValue == null) ? 0 : originalValue.hashCode());
		result = prime * result + ((payDate == null) ? 0 : payDate.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bill other = (Bill) obj;
		if (delayDays == null) {
			if (other.delayDays != null)
				return false;
		} else if (!delayDays.equals(other.delayDays))
			return false;
		if (dueDate == null) {
			if (other.dueDate != null)
				return false;
		} else if (!dueDate.equals(other.dueDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (originalValue == null) {
			if (other.originalValue != null)
				return false;
		} else if (!originalValue.equals(other.originalValue))
			return false;
		if (payDate == null) {
			if (other.payDate != null)
				return false;
		} else if (!payDate.equals(other.payDate))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Bill [id=" + id + ", text=" + text + ", originalValue="
				+ originalValue + ", dueDate=" + dueDate + ", payDate="
				+ payDate + ", delayDays=" + delayDays + "]";
	}

	@Column(name = "corrected_value", nullable=false)
	public BigDecimal getCorrectedValue() {
		return correctedValue;
	}

	public void setCorrectedValue(BigDecimal correctedValue) {
		this.correctedValue = correctedValue;
	}

	

}
