package com.deliverit.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.deliverit.entity.Bill;
import com.deliverit.service.BillService;
import com.deliverit.util.DeliveritBusinessException;
import com.deliverit.util.WarningType;

@Controller
@RequestMapping("/deliverit")
public class BillController {

	@Autowired
	private BillService billService;

	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Created.", response = List.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = DeliveritBusinessException.class) })
	@PostMapping("/save")
	@ResponseBody
	public void save(Bill bill) {
		billService.saveBill(bill);
	}

	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK.", response = List[].class),
			@ApiResponse(code = 403, message = "Forbidden", response = DeliveritBusinessException.class),
			@ApiResponse(code = 404, message = "Not Found", response = DeliveritBusinessException.class),
			@ApiResponse(code = 422, message = "Unprocessable Entity.", response = DeliveritBusinessException.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = DeliveritBusinessException.class) })
	@SuppressWarnings(WarningType.UNCHECKED)
	@GetMapping("/list-all")
	@ResponseBody
	public List<Bill> listAll() {
		Optional<List<Bill>> bills = billService.listAll();
		return bills.map(bill -> {
			return bill;
		}).orElseThrow(new DeliveritBusinessException(HttpStatus.NOT_FOUND));
	}

}
