package com.deliverit.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import om.deliveriit.repository.BillRepository;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.deliverit.entity.Bill;
import com.delivery.business.Rate;

@Service
public class BillServiceImpl implements BillService {

	@Autowired
	private Logger logger;

	@Autowired
	private ModelMapper modelMapper;	

	@Autowired
	private BillRepository billRepository; 

	@Override
	@Cacheable
	public Optional<List<Bill>> listAll() {
		List<Bill> collect = StreamSupport.stream(
				billRepository.findAll().spliterator(), false).collect(
				Collectors.toList());
		return Optional.ofNullable(collect);
	}

	@Override
	public void saveBill(Bill bill) {
		double totalToSum = bill.getDelayDays().getDays() + Rate.get(bill.getDelayDays()).getInterest();
		bill.setCorrectedValue(bill.getOriginalValue().add(percentValue(bill.getOriginalValue(), totalToSum)));
		billRepository.save(bill);
	}

	private static BigDecimal percentValue(BigDecimal originalValue, double percentage){
		return originalValue.multiply(new BigDecimal(percentage/100.0));
	}
}
