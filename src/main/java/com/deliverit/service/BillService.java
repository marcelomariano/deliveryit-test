package com.deliverit.service;

import java.util.List;
import java.util.Optional;

import com.deliverit.entity.Bill;

public interface BillService {

	void saveBill(Bill bill);
	
	Optional<List<Bill>> listAll();

}