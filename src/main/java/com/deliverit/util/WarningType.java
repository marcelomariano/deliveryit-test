package com.deliverit.util;

public interface WarningType {

	/**
	 * To suppress warnings relative to null.
	 */
	public static final String NULL 			= "null";

	/**
	 * To suppress warnings relative to deprecation.
	 */
	public static final String DEPRECATION 		= "deprecation";

	/**
	 *  To suppress warnings relative to finally block that don’t return.
	 */
	public static final String FINALLY 			= "finally";

	/**
	 * To suppress warnings relative to javadoc warnings.
	 */
	public static final String JAVADOC 			= "javadoc";

	/**
	 * To suppress warnings relative to usage of raw types.
	 */
	public static final String RAWTYPES 		= "rawtypes";

	/**
	 * To suppress warnings relative to missing serialVersionUID field for a serializable class.
	 */
	public static final String SERIAL 			= "serial";

	/**
	 * to suppress warnings relative to incorrect static access
	 */
	public static final String STATIC_ACCESS 	= "static-access";

	/**
	 * To suppress warnings relative to unchecked operations.
	 */
	public static final String UNCHECKED 		= "unchecked";

	/**
	 * To suppress warnings relative to unused code.
	 */
	public static final String UNUSED 			= "unused";

	/**
	 * To supress warnings relative to ambiguous dependency from cdi inject points.
	 */
	public static final String CDI_ANBIGUOUS_DEPENDENCY = "cdi-ambiguous-dependency";


}