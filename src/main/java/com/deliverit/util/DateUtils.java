package com.deliverit.util;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoField;
import java.time.temporal.ValueRange;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

	static Format format = new SimpleDateFormat("dd/MM/yyyy");

	public static Date addValidityDate(Date date, int validity) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, validity);
		return calendar.getTime();
	}

	public enum FirstLastDay{
		FIRST, LAST;
	}

	public static Date setDateOfMonth(Date date, FirstLastDay day){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int zero = 0;
		if (day == FirstLastDay.FIRST) {
			calendar.set(Calendar.DATE, 1);
		}else if (day == FirstLastDay.LAST) {
			LocalDate localDate = LocalDate.now();
			ValueRange range = localDate.range(ChronoField.DAY_OF_MONTH);
			Long max = range.getMaximum();
			LocalDate lastDay = localDate.withDayOfMonth(max.intValue());
			calendar.set(Calendar.DATE, lastDay.getDayOfMonth());
		}
		calendar.set(Calendar.HOUR, zero);
		calendar.set(Calendar.MINUTE, zero);
		calendar.set(Calendar.SECOND, zero);
		return calendar.getTime();
	}

	public static Date addTimeAppointment(Date startDate, int timeAppointment){
		Format dia = new SimpleDateFormat("dd/MM/yyyy");
		Format hora = new SimpleDateFormat("HH:mm");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		String horaInicial = hora.format(calendar.getTime());
		calendar.add(Calendar.MINUTE, timeAppointment);
		return calendar.getTime();
	}

	public static boolean isSameDate(Date date1, Date date2){
		Calendar c1 = Calendar.getInstance();
		c1.setTime(date1);
		Calendar c2 = Calendar.getInstance();
		c2.setTime(date2);

		return ((c1.get(Calendar.DATE) == c2.get(Calendar.DATE))
				&&
				(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH))
				&&
				(c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)));
	}

	public static Date setLastMinuteFromDay(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	public static Date setTimes(Date date, int hour, int minute, int second){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, second);
		return calendar.getTime();
	}

	public static boolean isDateTimeBeetwen(Date startDate, Date finishDate,
			Date startDateCompare, Date finishDateCompare) {

		if (
				// se as data-inicio e data-fim forem iguais as datas comparativas de inicio e fim
				(startDate.compareTo(startDateCompare) == 0 && finishDate
				.compareTo(finishDateCompare) == 0)
				||
				// se a data-inicio-comparativa for antes da data-inicio E
				// a data-fim-comparativa estiver entre a data-inicio e data-fim
				(startDateCompare.before(startDate) &&
						(finishDateCompare.after(startDate) && finishDateCompare.before(finishDate)))
				||
				// se a data-inicio-comparativa estiver entre a data-inicio e data-fim E
				// a data-fim-comparativa for depois da data-fim
				((startDateCompare.after(startDate) && startDateCompare.before(finishDate)) 
						&& finishDateCompare.after(finishDate))
				){
			return true;
		}
		return false;

	}

	public static Period periodBetween(LocalDate firstTime, LocalDate secondTime){
		return Period.between(firstTime, secondTime);
	}


}