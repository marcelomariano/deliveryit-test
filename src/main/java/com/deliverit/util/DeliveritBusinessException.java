package com.deliverit.util;

import java.util.function.Supplier;

import org.springframework.http.HttpStatus;

public class DeliveritBusinessException extends RuntimeException implements Supplier {

	private static final long serialVersionUID = -6125298283326234461L;

	private HttpStatus httpStatus;

	public DeliveritBusinessException () {
		super();
	}

	public DeliveritBusinessException (HttpStatus httpStatus, String message) {
		super(message);
		this.httpStatus = httpStatus;
	}

	public DeliveritBusinessException (HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public DeliveritBusinessException (String message) {
		super(message);
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	@Override
	public Object get() {
		return this;
	}


}
