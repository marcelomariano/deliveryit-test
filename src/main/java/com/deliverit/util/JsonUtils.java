package com.deliverit.util;

import java.io.Serializable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtils {

	private static final ObjectMapper MAPPER = new ObjectMapper();

	public static String logJson(Serializable body) {
		String content;
		try {
			content = MAPPER.writeValueAsString(body);
		} catch (JsonProcessingException | NullPointerException e) {
			content = null;
		}
		return content;
	}


}

