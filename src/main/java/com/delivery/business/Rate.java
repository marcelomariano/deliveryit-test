package com.delivery.business;

import java.time.Period;


public enum Rate {

	UNDER_3_DAYS(2D, 0.1D){

		@Override
		public double fines() {
			return this.getFines();
		}

		@Override
		public double interest() {
			return this.getInterest();
		}

	},
	OVER_3_DAYS(3D, 0.2D){

		@Override
		public double fines() {
			return this.getFines();
		}

		@Override
		public double interest() {
			return this.getInterest();
		}

	},
	OVER_5_DAYS(5D, 0.3D){

		@Override
		public double fines() {
			return this.getFines();
		}

		@Override
		public double interest() {
			return this.getInterest();
		}

	};
	private double 	fines;//multa
	private double  interest;//juros

	public abstract double 	fines();
	public abstract double  interest();
	
	public double getFines(){
		return fines;
	}
	
	public double getInterest(){
		return interest;
	}
	

	private Rate(double 	fines, double  interest) {
		this.fines 		= fines;
		this.interest 	= interest;
	}
	
	public static Rate get(Period lateDays){
        	if (lateDays.getDays() < 3) {
				return Rate.UNDER_3_DAYS;
			} else if (lateDays.getDays() > 3 && lateDays.getDays() < 5) {
				return Rate.OVER_3_DAYS;
			} else if (lateDays.getDays() > 5) {
				return Rate.OVER_5_DAYS;
			}
			return null;
	}


}