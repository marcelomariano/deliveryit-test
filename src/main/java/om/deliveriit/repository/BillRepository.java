package om.deliveriit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.deliverit.entity.Bill;

@Repository
public interface BillRepository extends CrudRepository<Bill, Long> {

	Iterable<Bill> findAll();

}
