package com.deliverit;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.deliverit.entity.Bill;
import com.deliverit.service.BillService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DeliveritApplicationTests {
	
	@Autowired
	@MockBean
	private BillService billService;
	
	Optional<List<Bill>> bills;

	@Before
	public void setUp() {
		bills = billService.listAll();
	}
	
	@Test
	public void contextLoads() {
		Mockito.when(bills.isPresent()).thenReturn(true);
	}

	@Test
	public void testingPayBillUNDER_3_DAYS(){
		Bill bill = Bill.getInstance()
		.withText("Test paying bill UNDER_3_DAYS")
		.withOriginalValue(new BigDecimal(100))
		.withDueDate(LocalDate.now().minusDays(1))
		.withPayDate(LocalDate.now());
		billService.saveBill(bill);
		Mockito.when(Objects.nonNull(bill)).thenReturn(true);
	}
	
	@Test
	public void testingPayBillOVER_3_DAYS(){
		Bill bill = Bill.getInstance()
		.withText("Test paying bill OVER_3_DAYS")
		.withOriginalValue(new BigDecimal(100))
		.withDueDate(LocalDate.now().minusDays(1))
		.withPayDate(LocalDate.now());
		billService.saveBill(bill);
		Mockito.when(Objects.nonNull(bill)).thenReturn(true);
	}
	
	@Test
	public void testingPayBillOVER_5_DAYS(){
		Bill bill = Bill.getInstance()
		.withText("Test paying bill OVER_5_DAYS")
		.withOriginalValue(new BigDecimal(100))
		.withDueDate(LocalDate.now().minusDays(1))
		.withPayDate(LocalDate.now());
		billService.saveBill(bill);
		Mockito.when(Objects.nonNull(bill)).thenReturn(true);
	}

}
